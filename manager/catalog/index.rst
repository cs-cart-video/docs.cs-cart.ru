Каталог и товары
----------------

Данный раздел посвящен возможностям платформы по созданию и редактированию товаров и категорий. 

.. toctree::
    :maxdepth: 2
    :glob:

    products/index
    filters/index

.. fancybox:: products/img/catalog_02.png
    :alt: Товары
